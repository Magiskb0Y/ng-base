const nameModule = "angTitleModule";
const nameComponent = "angTitle";
const template = require("./angTitle.template.html");
const style = require("./angTitle.style.css");
module.exports.name = nameModule;
module.exports.nameComponent = nameComponent;

angular.module(nameModule, [])
.component(nameComponent, {
    template: template,
    controller: [
        "$scope",
        AngTitleController
],
    controllerAs: "self",
    bindings: {
        name: "<",
        description: "<"
    }
});

function AngTitleController($scope) {
    let self = this;

    this.$onInit = function() {
        
    }
}
