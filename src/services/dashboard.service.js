
const nameModule = "dashboardServiceModule";
const nameService = "dashboardService";
module.exports.name = nameModule;
module.exports.nameService = nameService;

angular.module("mainApp").factory(nameService, () => {
    return new Service();
});

function Service() {
    this.message = "Hello World";
}

Service.prototype.doSomething = function() {
    console.log(this.message);
}