const angular = require("angular");
require("angular-sanitize");
require("angular-ui-router");

const homeModule = require("./routes/home/home.component");

angular.module("mainApp", [
    "ui.router",
    homeModule.name
])
.config(($stateProvider, $urlRouterProvider) => {
    $urlRouterProvider.otherwise("/");

    $stateProvider.state("home", {
        url: "/",
        component: homeModule.nameComponent
    })
});
