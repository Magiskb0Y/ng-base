const angTitleModule = require("../../components/angTitle/angTitle.component");

const nameModule = "homeModule";
const nameComponent = "home";
const template = require("./home.template.html");
const style = require("./home.style.css");
module.exports.name = nameModule;
module.exports.nameComponent = nameComponent;

angular.module(nameModule, [
    angTitleModule.name,
])
.component(nameComponent, {
    template: template,
    controller: [
        "$scope",
        HomeController,
    ],
    controllerAs: "self"
});

function HomeController($scope) {
    let self = this;

    this.$onInit = function() {
        self.title = {
            name: "AngularJS",
            description: "A simple AngularJS project"
        }
    }
}