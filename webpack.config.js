const path = require("path");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");


module.exports = {
    entry: "./src/main.module.js",
    mode: "development",
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "main.bundle.js"
    },
    watch: true,
    module: {
        rules: [{
            use: ["style-loader", "css-loader"],
            test: /\.css$/
        }, {
            use: ["html-loader"],
            test: /\.html$/
        },{
            test: /\.(jpe?g|png|gif|svg|ico)$/, 
            loader: [
                {
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        outputPath: "static/images/",
                        publicPath: "static/images/"
                    }
                }
            ]
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "src", "index.html"),
            filename: "index.html",
            hash: true
        }),
        new BrowserSyncPlugin({
            host: "localhost",
            port: 3000,
            server: {
                baseDir: path.resolve(__dirname, "./build")
            },
            // files: [path.resolve(__dirname, "src", "*.*")],
        }),
        new CleanWebpackPlugin(["build"])
    ]
}
